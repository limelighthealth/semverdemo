# **Limelight** ❤️ `semantic-release`

## **Abstract**

<https://semantic-release.gitbook.io/semantic-release/>

<https://www.conventionalcommits.org/en/v1.0.0-beta.4/>

Limelight Health should use `semantic-release` to **_automatically_**:
- **semantically version** all of its git repositories, fully, seamlessly
- **formalize** its entire distributed `git` commit history, using an industry standard format
- **generate formal release notes**, or at the least a starting release notes draft document
- **help to streamline** the release playbook

## **Background** (for posterity, skip)
As part of Sprint 0, Limelight elected to define a deployment/merge/test strategy for the Quotepad repository, as well as marking semantic versions for all of the libraries/executables in Limelight's total codebase. Additionally, other notable, important goals, such as identifying how to create a manifest of the application, and what release notes look like to hand off to professional services, were been identified.

As part of growing the engineering department's process into a more mature, automation CI/CD-friendly system, **this document, and this project in general, is a proposal to utilize `semantic-release` in combination with `bitbucket-pipelines` to help meet those desired Sprint 0 goals.**

### Limelight's selected branching model
Limelight's branching model can be viewed [here](https://docs.google.com/drawings/d1nz480mN1S4RaMhQeCNg0UIyBZ7CTo6cGsX-uR7BmNqo/edit). The intention is to only version on release branches.


## **Scope** (start here)

### **What** is `semantic-release`?
`Semantic-Release` describes itself as a tool that provides “fully automated version management and package publishing”. In addition to that, the relevant components of the feature set that **Limelight** could benefit from include:
- Enforce Semantic Versioning specification
- Notify maintainers and users of new releases
- Use formalized commit message convention to document changes in the codebase
- Publish on different distribution channels (such as npm dist-tags) based on git merges
- Integrate with your continuous integration workflow
- Avoid potential errors associated with manual releases
----

### **What** are the goals for Sprint 0 using `semantic-release`?

For Sprint 0, the focus of integrating `semantic-release` would be around:
1. git tagging for **semantic-version** annotation
2. automatic release notes doc generation, and 
3. introducing disciplined git commit behavior.

### **How** would Limelight integrate `semantic-release`?
`semantic-release` works by chewing through the git commit history diff between two versions of a codebase. The commits are analyzed for adherence to the git commit standard called `conventional-commits`, 
which requires inclusion of metadata in any given commit. 
That metadata is used to analyze how the codebase has grown and changed, 
and is subsequently able to identify what the `major`, `minor`, and `patch` version
 increments should be.

`semantic-release` is used in a **CI system**, such as **Travis** or **CirleCI.** 
It has integration support with many systems, 
including `bitbucket-pipelines`, 
which we will use in this proposal, and will ultimately be the 
suggested **CI system** for Limelight. 

----

### **Case Study**
This section highlights how the `semverDemo` project integrates
`semantic-release` in order to POC our semantic versioning
goals for sprint0.

#### Base Configuration

```yml
#file: bitbucket-pipelines.yml
image: node:10.15.0
pipelines:
  branches:
      release/*:
        - step:
            script:
              - npm install
              - npx semantic-release
```

In this simple example configuration, we are using `node v10` in order to run `semantic-release`. We only run this task when a `release/*` branch is created/updated in bitbucket. This tells `bitbucket-pipeline` to set up and run `semantic-release`, with the configuration set up in the `.releaserc` YML file.

```yml
#file: .releaserc
repositoryUrl: "https://ianrayllh@bitbucket.org/limelighthealth/semverdemo.git"
branches: release/*
plugins:
- "@semantic-release/commit-analyzer"
- "@semantic-release/release-notes-generator"
- "@semantic-release/changelog"
- "@semantic-release/git"
```

In this configuration, we're declaring that the remote URL we want to access is the `limelighthealth/semverdemo.git`. We declare that branches we're going to run this release task on must match the branch pattern `release/*`. Additionally, we're only going to run the **commit-analyzer, release-notes-generator, changelog, and git** tasks. This means we'll be able to compute a new semver, produce a release notes collection, a change log, and tag git with the new semver. We'll see what those all look like later on in this document. 

In order for the **HTTPS** connection to our `repositoryUrl`, in https://bitbucket.org/limelighthealth/semverdemo, we need to update the:

repository settings > pipelines settings > Repository variables to include a new variable: `GIT_CREDENTIALS=<userName>:<app-password for userName>`. 

_Note that app-passwords can be set up via user settings in the App Passwords option. In this case, we're using_ `GIT_CREDENTIALS=ianrayllh:<app-password for ianrayllh>`, _and we're marking it as protected in bitbucket._

Additionally, we need to install some dependencies in our `package.json`:

```bash
npm i -D commitizen 
npm i -D cz-conventional-changelog 
npm i -D semantic-release 
npm i -D @semantic-release/{git,commit-analyzer,release-notes-generator,npm,changelog}
```

and then let's modify our `package.json` to add some important stuff:
```json
{
  "scripts": {
    ...,
    "git-cz": "./node_modules/commitizen/bin/git-cz"
  },
  ...,
  "config": {
    "commitizen": {
      "path": "./node_modules/cz-conventional-changelog"
    }
  },
  ...
}
```
Let's take a look at what we've just added:
- `commitizen` is a convenience tool to help automate the writing of proper commit messages in `conventional-commit` format. It comes with a default binary `git-cz`, and we're just giving ourselves a simple way to invoke it, namely `npm run git-cz`. It would be equally simple to configure an environment path so that `git-cz` is available as a native terminal command.
- `cz-conventional-changelog` is a formatter for changelog creation.
- `@semantic-release/{...}` are the relevant plugins that we want to run in our environment.

**This represents the full scope of configuration necessary to get basic `semantic-release` off the ground and running.** Let's now take a look at how to use in practice.

----

#### `semantic-release` in the wild example
Let's say our repository ends up in practice with the following sequential git history:
- **Two features** are developed and merged into `master`.
- A **bugfix** for one of the features is merged into `master`.
- **A release `rc1` is cut from `master`.**
- Feature development continues on `master`, and **one new feature** is merged into `master`.
- A **hotfix** is created for `rc1`, which is dual merged into `rc1` and into `master`.
- **A release `rc2` is cut from `master`.**
- A **feature** with breaking changes is merged into `master`.
- **A release `rc3` is cut from `master`.**


Given this commit history, we should see that:
- `rc1` has semantic version `1.0.0`, then `1.0.1`
- `rc2` has semantic version `1.1.0`
- `rc3` has semantic version `2.0.0`
- release notes / change log are generated for each release.

By merging `release/*` back into `master` immediately, we properly
track the changelog as it should be. This step can be automated with further
modification to the `bitbucket-pipelines.yml` file, by adding
a git task after the` npx semantic-release` task:

```yml
...
script:
  - npm install
  - npx semantic-release
  - git fetch
  - git checkout master
  - git merge $BITBUCKET_BRANCH_NAME
  - git push origin master
```

----


### **What** else can we do with `semantic-release`?
- Since `semantic-release` is highly configurable, we can modify the way
the changelog file is produced, where/when/how we semantically version
- Since we manage all of our repositories in bitbucket, we can apply this same semver strategy
to `app-configs`, `librates`, `libquote`, `limelight-desktop`, etc. by configuring a `bitbucket-pipelines.yml` job.
- We can send out automatic notifications to our slack channels, emails, configure tasks to handle release failures, etc.

### **What** are the drawbacks to `semantic-release`?
- In the `.releaserc`, at the present moment it has only been successfully achieved using **HTTPS** to connect to bitbucket, and not yet **SSH**. This is likely due to the fact that Bitbucket is not a prime use case for `semantic-release`. In the future, there is likely to be better support or documentation on how to support **SSH** access for Bitbucket via `semantic-release`.
- There is an **initial cost associated with training our developers to commit differently**, with a cleaner
commit-message style. Currently, we have no discipline around
our git commit history. In order for this solution to work for us automatically,
we as an organization have to decide that the git history is valuable enough to us
to formalize.
- There is likely still investigation work necessary to make it work in **Quotepad**. For instance, a remaining task is to investigate how to seed the tag version that `semantic-release` tracks to increment the system.
