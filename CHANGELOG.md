# [3.2.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.1.3...v3.2.0) (2020-01-27)


### Bug Fixes

* **bitbucket-pipelines.yml:** finishing a release branch per OneFlow instructions ([d2c46ed](https://bitbucket.org/limelighthealth/semverdemo/commits/d2c46ed3f6a1ead8b96afc345fe257c5615deacf))
* **readme.md:** make hyperlinks clickable ([88343b5](https://bitbucket.org/limelighthealth/semverdemo/commits/88343b50d7850b8bc07cde57e8f1ecdc4ffdf6ca))


### Features

* "Kill all humans!" — Bender ([7514207](https://bitbucket.org/limelighthealth/semverdemo/commits/75142075730b2f832e26644336c350a1243888f8))
* "Kill all modern humans!" — Fry ([ca1caba](https://bitbucket.org/limelighthealth/semverdemo/commits/ca1caba69fd737113ea0ffa46a1964219b659332))
* **feature 7:** do feature 7 stuff ([2327c30](https://bitbucket.org/limelighthealth/semverdemo/commits/2327c3053bbc9cb627d085c5318772c207e1b069))

## [3.1.3](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.1.2...v3.1.3) (2020-01-27)


### Bug Fixes

* fix date value in FEATURE_FILE_6 by updating it ([8622819](https://bitbucket.org/limelighthealth/semverdemo/commits/86228191995a572e5d9aadc888ae63896d45d964))

## [3.1.2](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.1.1...v3.1.2) (2020-01-27)


### Bug Fixes

* **bitbucket-pipelines.yml:** fix merge conflicts via accept theirs merge strategy ([83b46ad](https://bitbucket.org/limelighthealth/semverdemo/commits/83b46ad74d2a805478b48d4fb99eb6bfe4b6f9c9))

## [3.1.1](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.1.0...v3.1.1) (2020-01-27)


### Bug Fixes

* **bitbucket-pipelines.yml:** fix merge conflict by accepting theirs ([df01d8c](https://bitbucket.org/limelighthealth/semverdemo/commits/df01d8c117c4a150dae883881dcef76c1092da2e))

# [3.1.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.0.2...v3.1.0) (2020-01-27)


### Features

* add FEATURE_FILE_6 ([d4b1421](https://bitbucket.org/limelighthealth/semverdemo/commits/d4b142140996d187f254f3b7680594fbc2a675e3))

## [3.0.2](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.0.1...v3.0.2) (2020-01-27)


### Bug Fixes

* finishing a release branch per OneFlow instructions ([65aa767](https://bitbucket.org/limelighthealth/semverdemo/commits/65aa767ed9922b979e4a13cf79aa3e96a7b632e0))

## [3.0.1](https://bitbucket.org/limelighthealth/semverdemo/compare/v3.0.0...v3.0.1) (2020-01-27)


### Bug Fixes

* **bitbucket-pipelines.yml:** finishing a release branch per OneFlow instructions ([127678d](https://bitbucket.org/limelighthealth/semverdemo/commits/127678d966887fb028c9160ce9aa009bcdc41185))

# [3.0.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v2.1.1...v3.0.0) (2020-01-27)


### Features

* hello ([bd46a5e](https://bitbucket.org/limelighthealth/semverdemo/commits/bd46a5e9fd4c375f48242da6cf9e33c8463761b6))


### BREAKING CHANGES

* A BREAKING CHANGE

## [2.1.1](https://bitbucket.org/limelighthealth/semverdemo/compare/v2.1.0...v2.1.1) (2020-01-27)


### Bug Fixes

* **bitbucket-pipelines.yml:** fix error: pathspec 'master' did not match any file(s) known to git ([dd23463](https://bitbucket.org/limelighthealth/semverdemo/commits/dd23463f89d24059e77c7e036e13a589d5714bf8))

# [2.1.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v2.0.0...v2.1.0) (2020-01-27)


### Features

* **feature_file_5:** fEATURE_FILE_5 ([2f0461d](https://bitbucket.org/limelighthealth/semverdemo/commits/2f0461dc2c31cac68f66b0ccfa3b3f4223dac1ba))

# [2.0.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v1.1.0...v2.0.0) (2020-01-26)


### Features

* **feature 1, feature 4:** implement feature 4, modify feature 1 ([4c03d4a](https://bitbucket.org/limelighthealth/semverdemo/commits/4c03d4a3131aded20eaa23b89f5bf16091d19d26))


### BREAKING CHANGES

* **feature 1, feature 4:** Feature 1 used to do "blah blah blah". Now, Feature 1 does "yadda yadda yadda" in a
way that is incompatible to Feature 1 consumers. We think this change is for the best though,
because "etc etc etc"

FAKE-6

# [1.1.0](https://bitbucket.org/limelighthealth/semverdemo/compare/v1.0.1...v1.1.0) (2020-01-26)


### Features

* **feature 3:** implement Feature 3 ([89e9a7e](https://bitbucket.org/limelighthealth/semverdemo/commits/89e9a7e8f79b91f09a67ea8157394ed098e84d64))

## [1.0.1](https://bitbucket.org/limelighthealth/semverdemo/compare/v1.0.0...v1.0.1) (2020-01-26)


### Bug Fixes

* **featuer 1, feature 2:** fix known issues in production ([4671f4a](https://bitbucket.org/limelighthealth/semverdemo/commits/4671f4a6dbca6eae3c67aef3324df62b673b8ba5))

# 1.0.0 (2020-01-26)


### Bug Fixes

* **feature 1:** fix bug in feature 1 ([30b32f4](https://bitbucket.org/limelighthealth/semverdemo/commits/30b32f4737c059a3b9908eaa79c52c564c3fc5e0))


### Features

* **feature 2:** implement Feature 2 ([9cdf3b8](https://bitbucket.org/limelighthealth/semverdemo/commits/9cdf3b85a28ee3c9347db6a81a5f8c0788f0080e))
* **feaure 1:** implement Feature 1 ([69bb504](https://bitbucket.org/limelighthealth/semverdemo/commits/69bb5048d30aab3635869400ce5fa5cba8282367))
